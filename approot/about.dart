import 'dart:io';
import 'package:path/path.dart' as path;
import 'package:ddblog/logger.dart';
import 'package:ddblog/appsupport/parcel.dart';
import 'package:ddblog/appsupport/remotelogger.dart';

// See also https://api.dartlang.org/stable/1.24.3/dart-isolate/Isolate/spawnUri.html
// 2-arg main() method can receive the 'message' from sender.
void main(List<String> args, Map<String, dynamic> isoParcel) {
  var request = RequestParcel(
    isoParcel[requestParcelKeyWebPath],
    isoParcel[requestParcelKeyParams],
    isoParcel[requestParcelKeySendPort]);
  var sendPort = request.sendPort;
  var log = Logger(
    path.basename(Platform.script.path),
    logWriter: RemoteLogWriter(sendPort),
    uncached: true);

  log
    ..n("Request received")
    ..d("Request received and logger created for isolated env");
  
  log.d("Deserialized OK");
  
  var date = DateTime.now();
  
  sendPort.send("ECHO: ${request.params} @ ${date.toString()}");
}