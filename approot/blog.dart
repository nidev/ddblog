import 'dart:io';
import 'package:path/path.dart' as path;
import 'package:markdown/markdown.dart' as markdown;
import 'package:ddblog/logger.dart';
import 'package:ddblog/servlet.dart';
import 'package:ddblog/appsupport/parcel.dart';
import 'package:ddblog/appsupport/remotelogger.dart';

// See also https://api.dartlang.org/stable/1.24.3/dart-isolate/Isolate/spawnUri.html
// 2-arg main() method can receive the 'message' from sender.
void main(List<String> args, Map<String, dynamic> isoParcel) async {
  var request = RequestParcel(
    isoParcel[requestParcelKeyWebPath],
    isoParcel[requestParcelKeyParams],
    isoParcel[requestParcelKeySendPort]);
  var sendPort = request.sendPort;
  var log = Logger(
    path.basename(Platform.script.path),
    logWriter: RemoteLogWriter(sendPort),
    uncached: true);
  
  var blogWebPath = request.webpath.replaceFirst("/blog", "/").replaceAll("//", "/");

  if (blogWebPath == "/") {
    blogWebPath = "/main";
  }

  // TODO: Path existence checking
  var markdownContentsFile = File(webSrcRootPath + "${blogWebPath}.md");
  var monolithTemplateFile = File(webSrcRootPath + "/monolith.html");
  var monolithTemplate = await monolithTemplateFile.readAsString();
  var markdownContents = await markdownContentsFile.readAsString();
  var renderedMarkdown = markdown.markdownToHtml(markdownContents);
  
  sendPort.send(ResponseParcel.serialize(monolithTemplate.replaceFirst("{{contents}}", renderedMarkdown), HttpStatus.ok, ContentType.html));
  sendPort.send(ClosingParcel.serialize());
}