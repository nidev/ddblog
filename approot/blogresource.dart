import 'dart:io';
import 'package:path/path.dart' as path;
import 'package:ddblog/logger.dart';
import 'package:ddblog/servlet.dart';
import 'package:ddblog/appsupport/parcel.dart';
import 'package:ddblog/appsupport/remotelogger.dart';

// See also https://api.dartlang.org/stable/1.24.3/dart-isolate/Isolate/spawnUri.html
// 2-arg main() method can receive the 'message' from sender.
void main(List<String> args, Map<String, dynamic> isoParcel) {
  var request = RequestParcel(
    isoParcel[requestParcelKeyWebPath],
    isoParcel[requestParcelKeyParams],
    isoParcel[requestParcelKeySendPort]);
  var sendPort = request.sendPort;
  var log = Logger(
    path.basename(Platform.script.path),
    logWriter: RemoteLogWriter(sendPort),
    uncached: true);
  
  var webResourcePath = request.webpath.replaceFirst("/resource", "/").replaceAll("//", "/");

  if (webResourcePath == "/") {
    webResourcePath = "/resource";
  }

  log.d(webResourcePath);

  // TODO: Path existence checking
  var trueResource = File(webSrcRootPath + "/resource/${webResourcePath}");

  if (trueResource.existsSync()) {
    // TODO: Can we pass a File object or path to a file instead of full data?
    // Due to passing full file over SendPort, size limitation is necessary.
    log.d("Serve file: ${trueResource.path}");

    sendPort.send(HttpServeParcel.serialize(trueResource.path));
  } else {
    sendPort.send(ResponseParcel.serialize("File not exist", HttpStatus.notFound, ContentType.text));
  }
}