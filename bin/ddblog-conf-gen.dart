// encoding: utf-8
import "dart:io";
import "package:path/path.dart" as Path;
import "package:ddblog/config.dart";

final String scriptName = Path.basename(Platform.script.path);
const List<String> supportedExtensions = const [".json", ".yaml"];

void main(List<String> arguments) {
  if (arguments.length == 0) {
    print("Configuration generator");
    print("Usage:");
    print("    ${scriptName} (filename).json");
    print("    ${scriptName} (filename).yaml");
    exit(255);
  }

  final String fileName = arguments[0];
  final String estimatedOutputFormat = Path.extension(fileName).toLowerCase();

  SSConfiguration config = SSConfiguration();

  switch (estimatedOutputFormat) {
    case ".json":
      config.writeToJSON(".", fileName).then((file) => "Saved");
      break;
    case ".yaml":
      config.writeToYAML(".", fileName).then((file) => "Saved");
      break;
    default:
      print("Unsupported format: ${estimatedOutputFormat}");
      exit(255);
  }

  print("Done");
}
