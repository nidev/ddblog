## 소개

돌피드림으로 시작하는 즐거운 인형술
(Delightful and brand-new days with Dollfie Dream(R))

이 프로젝트는 돌피드림을 처음 접하거나, 계속 데리고 다니면서 쌓인 경험들을 지식화하기위한 프로젝트입니다.

이 프로젝트의 구동 엔진인 ddblog는 [Google]()의 [Dart VM]() 2.0 개발 버전을 바탕으로 제작되었습니다.

## 주요 컨텐츠

### 인형 들여오기(adoption)

새로 시작하시는 분이신가요? 돌피드림을 들이기 전에 고려하셔야할 사항에 대해 정리하고, 결정에 도움이 될 수 있는 정보를 정리해두었습니다.

### 첫만남(initialinspect)

고민 끝에 돌피드림과의 첫 만남! 하지만 반가움 이전에, 즐거운 인형놀이 중 기분이 짜게 식는 일이 없도록 초기 불량 등 살펴보셔야할 부분을 정리해봅니다.

### 인형용품 구입(shopping)

기본 구성을 구입하시거나, 한정 구성을 구입하심으로써 처음엔 다른 분들과 같은 출발선에 서있게 될 것입니다. 하지만 앞으로 당신과 함께하는 돌피드림은 당신만의 것입니다. 인형과 함께 지내다보면 기존 옷이 지루해질 수도 있고, 계절에 맞춰 다른 옷을 입혀주거나, 즐거운 데이트를 위한 준비가 필요해질 수 있습니다.

그리고 사람이 씻고 이발도 하고 외모를 가꾸고 운동을 하여 항상 건강한 상태를 유지하는 것처럼, 돌피드림도 꾸준한 관리가 필요하기에 이런 용품들을 구입하는 경로를 정리해봅니다.

### [인형과의 외출(odekake)](odekake/odekake)

사람도 집안에만 지내면 답답한 것처럼, 이왕 돌피드림이 생겼으니 이전에 혼자 다녀와본 곳이더라도 인형과 함께 외출하여 색다른 시간을 보내고, 즐거운 추억을 만들어보시기 바랍니다.

단거리/장거리, 국내, 국외(아시아/유럽/미주)에 인형을 함께 데리고 가실 때 주의하실 부분이나 촬영 팁을 정리해봅니다.

#### [사진찍기(photograph)](photograph/photograph)

친구들이나 지인들에게 인형과의 즐거운 외출 시간을 자랑하고 싶어도, 백날 입으로 떠들어봤자 많은 도움이 되진 않습니다. 역시 다녀왔다면, 남는 건 사진입니다. 실내와 실외, 외출 중에 사진 찍을 때 참고하실 수 있는 정보를 정리해두었습니다.

이 챕터는 특히 사진술과 관련된 내용을 위주로 정리하였으며, 일반적인 사항에 대해서는 '인형과의 외출' 부분을 참고해주시기 바랍니다.

### [건강관리(maintenance)](maintenance/maintenance)

돌피드림도 아플 때가 있습니다. 혹은 아픈 모습을 가지지 않도록, 사람처럼 챙겨줄 필요성이 있습니다. 항상 새 것과 비슷한 상태를 유지하기위해 수시로 관리를 해주어야 합니다. 관리 중점 사항들을 정리해봅니다.

## 라이센스 및 컨텐츠 이용에 대하여

### DD Blog Toolkit

툴킷의 라이센스는 [BSD License](https://opensource.org/licenses/BSD-3-Clause)를 따릅니다. 라이센스를 준수하는한, 상업적 이용을 포함한 모든 이용이 가능하지만 원 소스 작성자는 기술적 지원에 대한 보장을 하지 않습니다. 별도의 기술 지원이 필요한 경우에는 소정의 경로를 통해 요청해주시기 바랍니다. 급하지 않은 경우, GitHub 이슈 트래커에 이슈를 기재해주시기 바랍니다.

### Blog contents

돌피드림 관련하여 저자가 작성한 내용은 [Creative Commons]()를 따릅니다. 컨텐츠에 대한 이슈도 이슈 트래커에 남기실 수 있습니다만, 기술적인 부분이 아닌 경우 ddblog 내의 [ArtiBack]() 기능을 사용하셔서 피드백 이력을 남겨주시기 바랍니다.