import "package:ddblog/runner/base.dart";
import "package:ddblog/servlet.dart";

import "dart:mirrors";

/// 
class DevelopmentRunner extends Runner {
  int _maximumCount;

  @override
  void springup(ServletInterface servlet) async {
    // Show alerts using all log modes
    const symbols = const [#n, #d, #w, #n];
    var loggerMirror = reflect(servlet.log);

    for (var sym in symbols) {
      loggerMirror.invoke(sym, const ["*** DEVELOPMENT MODE ENABLED ***"]);
      loggerMirror.invoke(sym, const ["*** Please do not use this build/configuration on production ***"]);
    }

    await for (var msg in servlet.serve()) {
      print(msg);
    }
  }
}