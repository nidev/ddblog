import "package:ddblog/runner/base.dart";
import "package:ddblog/servlet.dart";


/// 
class CountingRunner extends Runner {
  final int _maximumCount;

  CountingRunner(this._maximumCount);

  @override
  void springup(ServletInterface servlet) async {
    servlet.log.w("*** YOU ARE USING COUNTING RUNNER ***");
    servlet.log.w("*** After ${_maximumCount} request(s), runner stops servlet ***");
    
    var counter = 0;

    await servlet.serve().take(_maximumCount).forEach((b) {
      if (b.item1 == ServletMessageType.isoappRouted) {
        counter++;
        servlet.log.n("[${counter}/${_maximumCount}] Process result: ${b}");
      }
    });

    servlet.log.n("*** Counting over. ***");
    servlet.halt(forcefully: true);
  }
}