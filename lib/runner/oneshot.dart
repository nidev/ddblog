import "package:ddblog/runner/base.dart";
import "package:ddblog/servlet.dart";


///  
class OneshotRunner implements Runner {
  @override
  void springup(ServletInterface servlet) async {
    servlet.log.w("*** YOU ARE USING ONESHOT RUNNER ***");
    servlet.log.w("*** Once a request is retrieved, runner stops servlet ***");

    await servlet.serve().firstWhere((b) => b.item1 == ServletMessageType.isoappRouted);

    servlet.log.n("Oneshot runner has receieved one callback result.");
    servlet.log.n("Quit.");
    servlet.halt(forcefully: true);
  }
}