import "package:ddblog/servlet.dart";


/// Skeleton class of Runner model 
abstract class Runner {
  void springup(ServletInterface servlet);
}