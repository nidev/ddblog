import "package:ddblog/runner/base.dart";
import "package:ddblog/servlet.dart";

import "dart:mirrors";

/// 
class DefaultRunner extends Runner {
  @override
  void springup(ServletInterface servlet) async {
    await servlet.serve().drain();
  }
}