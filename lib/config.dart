import "dart:convert";
import "dart:io";
import "dart:async";
import "package:ddblog/platform_util.dart";
import "package:ddblog/logger.dart";
import "package:yaml/yaml.dart" as Yaml;
import "package:path/path.dart" as Path;

const _loggerTag = "Config";
const cfgBindHost = "bind.host";
const cfgBindPort = "bind.port";
const cfgLoglevels = "log.levels";
const cfgWebSource = "web.sourcepath";
const cfgWebRoot = "web.rootpath";

const cfgAllKeyList = const [
  cfgBindHost,
  cfgBindPort,
  cfgLoglevels,
  cfgWebSource,
  cfgWebRoot
];

class SSConfiguration {
  String _cfgFilePath;
  Map<String, Object> _configMap;

  SSConfiguration() {
    _cfgFilePath = ":memory:";

    // Initializing
    _configMap = Map<String, Object>.fromIterable(cfgAllKeyList,
      key: (anyObject) => anyObject.toString(),
      value: (anyObject) => ""
    );
  }

  SSConfiguration.fromFile(String filePath) {
    _cfgFilePath = filePath;

    var contents = _readFile();
    var fileExtention = Path.extension(filePath).toLowerCase();

    if (fileExtention == ".json") {
      var jsonDecoder = JsonDecoder();
      _configMap = jsonDecoder.convert(contents);
    }
    else if (fileExtention == ".yaml") {
      _configMap = Yaml.loadYaml(contents);
    }
    else {
      throw Exception("Unsupported file extension. Should be either .json or .yaml");
    }

    if (!_validateConfigMap(_configMap)) {
      throw Exception("Configuration validation failed. See above warnings.");
    }
  }

  bool _validateConfigMap(Map<String, Object> map) {
    const logTagValidator = "CfgValidator";
    var log = Logger(logTagValidator);

    List<String> missingConfigs = [];
    List<String> unsetConfigs = [];

    cfgAllKeyList.forEach((key) {
      if (!map.containsKey(key)) {
        log.e("Missing required key: ${key}");
        missingConfigs.add(key);
      }
    });

    map.keys.forEach((key) {
      if (!cfgAllKeyList.contains(key)) {
        log.w("Unused key: ${key}");
        unsetConfigs.add(key);
      }
    });

    return (missingConfigs.length == 0);
  }

  String _readFile() {
    File srcFile = File(_cfgFilePath);

    if (srcFile.existsSync()) {
      return srcFile.readAsStringSync();
    }
    else {
      throw Exception("File not exist for loading configuration: ${_cfgFilePath}");
    }
  }

  Future<File> writeToJSON(String dir, String filename) async {
    File targetFile = File(Path.join(dir, filename));
    Logger log = Logger(_loggerTag);

    if (await targetFile.exists()) {
      log.w("File exists. Writing configuration will overwrite the file: ${filename}");
    }

    JsonEncoder encoder = JsonEncoder.withIndent("  ", (unencodable) => "");

    String serialized = encoder.convert(_configMap).replaceAll("\n", PlatformUtil.newLine);

    return targetFile.writeAsString(serialized,
      mode: FileMode.write,
      encoding: Encoding.getByName("UTF-8"),
      flush: true);
  }

  Future<File> writeToYAML(String dir, String filename) async {
    File targetFile = File(Path.join(dir, filename));
    Logger log = Logger(_loggerTag);

    if (await targetFile.exists()) {
      log.w("File exists. Writing configuration will overwrite the file: ${filename}");
    }

    StringBuffer stringBuffer = StringBuffer();
    _configMap.forEach((key, value) {
      stringBuffer.write("${key}: ${value.toString()}");
      stringBuffer.write(PlatformUtil.newLine);
    });

    return targetFile.writeAsString(stringBuffer.toString(),
      mode: FileMode.write,
      encoding: Encoding.getByName("UTF-8"),
      flush: true);
  }

  
  /// If value can be converted to number, this will return number type.
  /// In other cases, this function returns String with no exception.
  dynamic operator[](String key) {
    RegExp goodNumber = RegExp(r"^[0-9]+$");
    String value = _configMap[key].toString();

    if (goodNumber.hasMatch(value)) {
      return int.parse(value);
    }

    return value;
  }
}
