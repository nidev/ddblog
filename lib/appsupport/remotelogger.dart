
/// Remote log writer or centralized log writer
 

import "dart:isolate";
import "package:ddblog/appsupport/parcel.dart";
import "package:ddblog/log_writer/interface.dart";


/// 
class RemoteLogWriter implements LogWriter {
  SendPort _sendPort;

  RemoteLogWriter(this._sendPort);

  @override
  void write(String msg) {
    _sendPort.send(RemoteLogParcel.serialize(msg));
  }

  @override
  void writeLn(String msg) {
    write(msg);
  }

}