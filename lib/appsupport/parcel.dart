
/// Parcel helper collections 
/// Serializer/Deserializer for exchanging data
/// between isolated environment
/// 
/// Every Parcel is Map<String, dynamic> type,
/// Except 'SendPort' object, every value in Parcel MUST BE String type
/// 
/// when nested Map<K,V> object is passed to isolated context.
/// Serialize Map<K,V> into JSON, please.
/// So, 2nd parameter 'params' which type is Map<String, String>
/// is changed to serialized String (JSON format)
import "dart:isolate";
import "dart:convert";
import "dart:io";

const String parcelKeyTag = "parceltag";
const String requestParcelKeyWebPath = "webpath";
const String requestParcelKeyParams = "params";
const String requestParcelKeySendPort = "sendport";
const String requestParcelTag = "reqparcel";
const String responseParcelKeyContentType = "contenttype";
const String responseParcelKeyData = "responsedata";
const String responseParcelKeyHttpStatus = "httpstatus";
const String responseParcelTag = "resparcel";
const String sysinfoParcelKeyVersion = "version";
const String sysinfoParcelKeyServletName = "servletname";
const String sysinfoParcelKeyPlatform = "platform";
const String sysinfoParcelTag = "sysinfoparcel";
const String remotelogParcelKeyLog = "logcontents";
const String remotelogParcelTag = "logparcel";
const String closingParcelTag = "flushAndClose";
const String httpServeParcelKeyFilePath = "filepath";
const String httpServeParcelTag = "serveOverHttp";

// Serializer/Deserializer for parameter Map's.
JsonEncoder _jsonEncoder = JsonEncoder();
JsonDecoder _jsonDecoder = JsonDecoder();


/// 
class NoDefaultConstructor implements Exception {
  String cause;

  NoDefaultConstructor(String msg) {
    cause = msg;
  }
}


/// 
class BaseParcel { }


/// 
class RequestParcel extends BaseParcel {
  String _webpath;
  String _serializedParams;
  SendPort _sendPort;

  String get webpath => _webpath;
  Map<String, dynamic> get params => _jsonDecoder.convert(_serializedParams);
  SendPort get sendPort => _sendPort;

  
  /// Deserialize
  RequestParcel(this._webpath, this._serializedParams, this._sendPort);

  
  /// Serialize
  static Map<String, dynamic> serialize(String webpath, Map<String, String> params, SendPort sendPort) {
    return <String, dynamic>{
      parcelKeyTag: requestParcelTag,
      requestParcelKeyWebPath: webpath,
      requestParcelKeyParams: _jsonEncoder.convert(params),
      requestParcelKeySendPort: sendPort
    };
  }
}


/// 
class ResponseParcel extends BaseParcel {
  String _data;
  ContentType _contentType;
  int _httpStatus;

  String get data => _data;
  String get contentTypeString => _contentType.toString();
  ContentType get contentType => _contentType;
  int get httpStatus => _httpStatus;
  
  
  /// Deserialize
  ResponseParcel(this._data, this._httpStatus, this._contentType);

  
  /// Serialize
  static Map<String, dynamic> serialize(String data, int statusCode, ContentType contentType) {
    return <String, dynamic>{
      parcelKeyTag: responseParcelTag,
      responseParcelKeyData: data,
      responseParcelKeyHttpStatus: statusCode,
      responseParcelKeyContentType: contentType.toString()
    };
  }
}


/// 
class SysinfoParcel extends BaseParcel {
  String _platform;
  String _servletName;
  String _versionString;

  String get platform => _platform;
  String get servletName => _servletName;
  String get versionString => _versionString;
  
  
  /// Deserialize
  SysinfoParcel(this._platform, this._servletName, this._versionString);

  
  /// Serialize
  static Map<String, dynamic> serialize(String platform, String servletName, String versionString) {
    return <String, dynamic>{
      parcelKeyTag: sysinfoParcelTag,
      sysinfoParcelKeyPlatform: platform,
      sysinfoParcelKeyServletName: servletName,
      sysinfoParcelKeyVersion: versionString
    };
  }
}


/// 
class RemoteLogParcel extends BaseParcel {
  String _log;

  String get log => _log;
  
  
  /// Deserialize
  RemoteLogParcel(this._log);

  
  /// Serialize
  static Map<String, dynamic> serialize(String log) {
    return <String, dynamic>{
      parcelKeyTag: remotelogParcelTag,
      remotelogParcelKeyLog: log
    };
  }
}

/// 
class HttpServeParcel extends BaseParcel {
  String _filePath;

  String get filePath => _filePath;
  
  
  /// Deserialize
  HttpServeParcel(this._filePath);

  
  /// Serialize
  static Map<String, dynamic> serialize(String filePath) {
    return <String, dynamic>{
      parcelKeyTag: httpServeParcelTag,
      httpServeParcelKeyFilePath: filePath
    };
  }
}

/// Parcel that indicates an isolated app is about to finish its job and close connection.
/// No parameter is required.
 
class ClosingParcel extends BaseParcel {
  
  /// Deserialize
  ClosingParcel();

  
  /// Serialize
  static Map<String, dynamic> serialize() {
    return <String, dynamic>{
      parcelKeyTag: closingParcelTag
    };
  }
}