import 'dart:async';
import 'dart:io';
import 'dart:isolate';
import 'package:tuple/tuple.dart';
import 'package:path/path.dart' as path;
import 'package:mime/mime.dart' as mime;
import 'package:ddblog/config.dart';
import 'package:ddblog/logger.dart';
import 'package:ddblog/appsupport/parcel.dart';

/// Global paths
const isoAppRootFolderName = "approot";
const webSrcRootFolderName = "webSrc";

String get isoAppRootPath {
  var isoAppRootPathSegments = List<String>.of(Platform.script.pathSegments)
    ..removeLast() // remove ddblog.dart
    ..removeLast() // remove bin
    ..add(isoAppRootFolderName); // add path to approot

  // Do not use Platform.pathSeparator to join
  // Isolate accepts only / both Windows and *nix.
  return isoAppRootPathSegments.join("/");
}

String get webSrcRootPath {
  var isoAppRootPathSegments = List<String>.of(Platform.script.pathSegments)
    ..removeLast() // remove ddblog.dart
    ..removeLast() // remove bin
    ..add(webSrcRootFolderName); // add path to websrc

  // Do not use Platform.pathSeparator to join
  // Isolate accepts only / both Windows and *nix.
  return isoAppRootPathSegments.join("/");
}

/// ServletMessageType enum
enum ServletMessageType {
  connectionAccepted,
  isoappRouted,
  isoappNotRouted,
}

/// Servlet Core class
abstract class ServletInterface {
  Logger _log;
  Logger _remoteLog;
  Logger get log => _log;
  Logger get remoteLog => _remoteLog;

  Stream<Tuple2<ServletMessageType, String>> serve();
  void halt({bool forcefully = false});
}

/// Servlet class
class Servlet extends ServletInterface {
  SSConfiguration config;
  InternetAddress bindHost = InternetAddress.anyIPv6;
  int bindPort = 8000;

  Servlet(Logger logger, SSConfiguration configuration) {
    _log = logger;
    _remoteLog = Logger("Remote");
    config = configuration;
  }

  @override
  Stream<Tuple2<ServletMessageType, String>> serve() async* {
    // TODO: Separate as a class
    // or make an XML table
    const Map<String, String> routeMapDefinition = const {
      "^(/|/index)\$": "index.dart",
      "^(/favicon.ico)\$": "favicon.dart",
      "^(/blog)": "blog.dart",
      "^(/resource/)": "blogresource.dart",
      "^(/about)\$": "about.dart",
    };

    // XXX: Avoid hard wiring, let's make it flexible
    const String routeFallbackIsoAppPath = "fallback.dart";

    log.d("Routing configuration:");

    routeMapDefinition.forEach((regexStringPath, isoAppPath) {
      log.d("\t${regexStringPath} ==> ${isoAppPath}");
    });

    log.d("Routing configurations are begin compiled...");

    var routeMapCompiled = Map.unmodifiable(
      routeMapDefinition.map((regexString, isoAppPath) => MapEntry<RegExp, String>(
        RegExp(regexString), "${isoAppRootPath}/${isoAppPath}")
      )
    ).cast<RegExp, String>();

    log.d("Routing configuartions are compiled.");

    log.n("Start http server on ${bindHost}:${bindPort}");

    try {
      var httpServer = await HttpServer.bind(bindHost, bindPort, shared: true);
      
      log
        ..d("Connection binding information:")
        ..d("\tBind on ${httpServer.address}")
        ..d("\tUse port number ${httpServer.port}");
      
      await for (HttpRequest req in httpServer) {
        yield Tuple2<ServletMessageType, String>(ServletMessageType.connectionAccepted, "Connection Accepted");

        var paramMap = req.uri.queryParameters;
        var webpath = path.normalize(req.uri.path);

        if (Platform.isWindows) {
          webpath = webpath.replaceAll("\\", "/");
        }

        var routeEntry = routeMapCompiled.entries.firstWhere((entry) {
          var pathMatched = entry.key.firstMatch(webpath);
          return pathMatched != null && pathMatched.groupCount > 0;
        }, orElse: null);
        var routedIsoApp = routeFallbackIsoAppPath;

        if (routeEntry == null) {
          yield Tuple2<ServletMessageType, String>(ServletMessageType.isoappNotRouted, "No route found. Fallback to default.");
        } else {
          yield Tuple2<ServletMessageType, String>(ServletMessageType.isoappRouted, "IsoApp routed");
          
          routedIsoApp = routeEntry.value;
        }
          
        log
          ..n("Welcome, ${webpath} from ${req.connectionInfo.remoteAddress}")
          ..d("Remote connection information:")
          ..d("\tRemote address: ${req.connectionInfo.remoteAddress}")
          ..d("\tRemote Port: ${req.connectionInfo.remotePort}");
        
        var receiverToIsoApp = ReceivePort();
        
        receiverToIsoApp
        .listen((dynamic msg) async {
          log.n("Parcel received from ${routedIsoApp}, hashCode is ${msg.hashCode}");

          if (msg is String) {
            req.response.statusCode = HttpStatus.ok;
            req.response.write(msg);
            req.response.write("\r\n\r\n");
            await req.response.close();
          } else if (msg is Map) {
            var parcel = msg as Map<String, dynamic>;
            
            if (!parcel.containsKey(parcelKeyTag)) {
              log
                ..e("Invalid Parcel was sent from ${routedIsoApp} and could not be processed. Ignored.")
                ..d("Receiver took untagged Parcel from ${routedIsoApp}")
                ..d("Raw contents:")
                ..d("\t${parcel}");
            } else {
              var parcelTag = parcel[parcelKeyTag] as String;

              if (parcelTag == responseParcelTag) {
                // response
                var responseParcel = ResponseParcel(
                  parcel[responseParcelKeyData],
                  parcel[responseParcelKeyHttpStatus],
                  ContentType.parse(parcel[responseParcelKeyContentType]));

                req.response.statusCode = responseParcel.httpStatus;
                req.response.headers.contentType = responseParcel.contentType;
                req.response.write(responseParcel.data);
                req.response.write("\r\n\r\n");

                await req.response.close();
              } else if (parcelTag == sysinfoParcelTag) {
                // sysinfo
                throw UnimplementedError("Not implemented yet");
              } else if (parcelTag == remotelogParcelTag) {
                var remotelogParcel = RemoteLogParcel(parcel[remotelogParcelKeyLog]);

                // Use raw logging mode, which just passes messages and does not print time information
                remoteLog.r(remotelogParcel.log);
              } else if (parcelTag == httpServeParcelTag) {
                // httpserve
                var httpServeParcel = HttpServeParcel(parcel[httpServeParcelKeyFilePath]);

                // Check file existence and serve it drectly over HTTP
                // TODO: should check the path. The path can not locate a file at the outside of the WEBSRC
                var rawResourcePath = httpServeParcel.filePath;

                // TODO: Path existence checking
                var resource = File(rawResourcePath);
                var resourceStat = await resource.stat();

                if (resourceStat.type == FileSystemEntityType.file) {
                  // TODO: Can we pass a File object or path to a file instead of full data?
                  // Due to passing full file over SendPort, size limitation is necessary.
                  const maximumFileSize = 1 << 15; // 32kB
                  const transferBlockSize = 1 << 10; // 1024B = 1kB

                  // Obtain content size
                  var contentLength = resourceStat.size;

                  if (contentLength > maximumFileSize) {
                    // Feeding back to origin SendPort 
                    receiverToIsoApp.sendPort.send(ResponseParcel.serialize("File size limit exceeded", HttpStatus.internalServerError, ContentType.text));
                  } else {
                    var contentType = ContentType.binary;
                    var guessedMimeType = mime.lookupMimeType(resource.path);

                    if (guessedMimeType != null) {
                      contentType = ContentType.parse(guessedMimeType);
                    }

                    req.response.headers.clear();
                    req.response.headers.add(HttpHeaders.contentTypeHeader, contentType.toString());
                    req.response.headers.add(HttpHeaders.contentLengthHeader, contentLength);
                    
                    await req.response.addStream(resource.openRead());
                    await req.response.flush();
                    await req.response.close();
                  }
                } else {
                  // Feeding back to origin SendPort 
                  receiverToIsoApp.sendPort.send(ResponseParcel.serialize("File not exist", HttpStatus.notFound, ContentType.text));
                }
              } else if (parcelTag == closingParcelTag) {
                // close port
                receiverToIsoApp.close();
                await req.response.close();

                log
                  ..n("Closing bridge from isolated app ${routedIsoApp}, also close HttpResponse object")
                  ..d("SendPort bridge is closed and does not accept additional messages.");
              } else {
                throw UnsupportedError("Unsupported Parcel tag ${parcelTag} from ${routedIsoApp}");
              }
            }
          } else {
            log.d("Unhandled type, received: ${msg.runtimeType}");
            log.d("==== Contents begin ====");
            log.d("${msg}");
            log.d("==== Contents end   ====");
          }
        })
        .onError((dynamic error) async {
          log
            ..e("Error while processing request from ${routedIsoApp}")
            ..d("Error from ${routedIsoApp}, Reason: ${error}")
            ..d("Parameter information:")
            ..d("\t${paramMap}")
            ..d("\t\n")
            ..d("===== End of params =====");

          req.response.statusCode = HttpStatus.internalServerError;
          req.response.write("Error while processing your request");
          await req.response.close();
        });

        var isoApp = Isolate.spawnUri(
          Uri.parse(routedIsoApp),
          [],
          RequestParcel.serialize(webpath, paramMap, receiverToIsoApp.sendPort),
          onError: receiverToIsoApp.sendPort,
          onExit: receiverToIsoApp.sendPort
        );

        isoApp
        .then((isolateEnv) async {
          log.n("Processing request: ${webpath}, params: ${paramMap}");
        })
        .catchError((error) async {
          log.e("Error on processing: ${webpath}, params: ${paramMap}");
          log
            ..d("Error on processing: ${webpath}, Error: ${error}")
            ..d("Trace:")
            ..d("\tisoApp path: ${routedIsoApp}")
            ..d("\tparams: ${paramMap}")
            ..d("===== End of trace =====");
          
          req.response.statusCode = HttpStatus.internalServerError;
          req.response.write("Error while processing your request");
          await req.response.close();
        });
      } // await for (HttpRequest req in httpServer)
    } catch (error, stackTrace) {
      log.e("Error on listening connection: ${error}");
      log.d("Stack trace for an error on listening connection: ${error}");
      log.d(stackTrace.toString());
    }
  }

  /// Halt servlet and dd blog tookit
  @override
  void halt({bool forcefully = false}) {
    // STUB
    // TODO: Implement and define proper procedures to halt service
    log
      ..n("Servlet now halts...")
      ..d("Servlet halting requested. (Force flag: ${forcefully})");
    
    exit(0);
  }
}