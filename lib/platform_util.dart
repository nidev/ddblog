// encoding: utf-8
import "dart:io";


const _newlineWin = "\r\n";
const _newlinexNix = "\n";

 /// Helper class for platform related operations lie file checking, R/W checking.
 /// And also provides platform-dependent values like newline character.
class PlatformUtil {
  static String get newLine => Platform.isWindows ? _newlineWin : _newlinexNix;
  
  /// Check file existency with synchronous calling
  /// May throw error(s) while calling underlying OS APIs.
  static bool exists(String path) {
    throw UnimplementedError();
  }

  
   /// Check file readability with synchronous calling
   /// May throw error(s) while calling underlying OS APIs.
  static bool readable(String path) {
    throw UnimplementedError();
  }

  /// Check file writability with synchronous calling
  /// May throw error(s) while calling underlying OS APIs.
  static bool writable(String path) {
    throw UnimplementedError();
  }

  
  /// Check file readability and writability with synchronous calling
  /// May throw error(s) while calling underlying OS APIs.
  static bool checkRW(String path) {
    return readable(path) && writable(path);
  }

  
  /// Obtain available free space of disk where tookit runs with synchronous calling
  /// May throw error(s) while calling underlying OS APIs.
  static BigInt obtainFreeSpace() {
    throw UnimplementedError();
  }
}