import "dart:convert";
// XXX: Broken library on Dart 2, does not support legacy form of
// handling noSuchMethod()
// import "package:colorize/colorize.dart";
import "package:ddblog/log_writer/interface.dart";
import "package:ddblog/log_writer/defaultwriter.dart";

const String TAG = "Logger";
enum LogLevel { debug, notify, error, warn }
enum LogDateMode { fulldate, none }
const Map<String, LogLevel> LogLevelFromString = const {
  "debug": LogLevel.debug,
  "notify": LogLevel.notify,
  "error": LogLevel.error,
  "warn": LogLevel.warn
};

class Logger {
  String _internalTag;
  LogWriter _logWriter;
  
  static final LineSplitter splitter = LineSplitter();
  static final Map<String, Logger> _singletons = Map<String, Logger>();
  static final Set<LogLevel> logLevels = Set<LogLevel>.from(LogLevel.values);

  // XXX: Broken library on Dart 2, does not support legacy form of
  // handling noSuchMethod()
  // static final Map<String, Colorize> _logTag = {
  //   "notify": Colorize("NOTIFY").apply("bgWhite").apply("black"),
  //   "debug": Colorize("DEBUG").apply("bgWhite").apply("blue"),
  //   "warn": Colorize("WARN").apply("yellow"),
  //   "error": Colorize("ERROR").apply("bgBlack").apply("red")
  // };
  static final Map<String, String> _logTag = {
    "notify": "NOTIFY",
    "debug": "DEBUG",
    "warn": "WARN",
    "error": "ERROR"
  };

  Logger._internal(this._internalTag, this._logWriter);

  factory Logger(String tag, { LogWriter logWriter = null, bool uncached = false }) {
    if (!_singletons.containsKey(tag)) {
      // If there's no given custom writer, use default one. (STDOUT)
      Logger new_logger = Logger._internal(tag, logWriter ?? DefaultLogWriter());
      
      new_logger.r("Logger for '${tag}' has been initialized");
      new_logger.r("At this time, following log levels are enabled: ${logLevels.toString()}");

      if (uncached) {
        new_logger.r("Uncached object creation. Logger '${tag}' will not be cached");

        return new_logger;
      }

      _singletons[tag] = new_logger;
    }
    
    return _singletons[tag];
  }

  void _l(String msg, { LogDateMode dateMode = LogDateMode.fulldate }) {
    switch (dateMode) {
      case LogDateMode.fulldate:
        _logWriter.writeLn("${_internalTag}|${DateTime.now()} ${msg}");
        break;
      case LogDateMode.none:
        _logWriter.writeLn("${_internalTag}| ${msg}");
        break;
      default:
        throw ArgumentError("Unhanlded LogDateMode type: ${dateMode}, MUST be fixed!");
    }
  }

  void d(String msg) {
     if (!logLevels.contains(LogLevel.debug)) return;

    splitter.convert(msg).forEach((msgLine) => _l("${_logTag['debug']}  ${msgLine}"));
  }

  void n(String msg) {
    if (!logLevels.contains(LogLevel.notify)) return;

    splitter.convert(msg).forEach((msgLine) => _l("${_logTag['notify']}  ${msgLine}"));
  }

  void e(String msg) {
    if (!logLevels.contains(LogLevel.error)) return;

    splitter.convert(msg).forEach((msgLine) => _l("${_logTag['error']}  ${msgLine}"));
  }

  void w(String msg) {
    if (!logLevels.contains(LogLevel.warn)) return;

    splitter.convert(msg).forEach((msgLine) => _l("${_logTag['warn']}   ${msgLine}"));
  }

  /// r method supports printing out unprocessed text.
  /// No date information will be appended, but only logger tag will be displayed.
  /// Intended for servlet remote logger interface, which just prints out received logs.
  void r(String msg) {
    _l(msg, dateMode: LogDateMode.none);
  }
}
