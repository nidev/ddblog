library runner;

import "package:ddblog/runner/base.dart";
import "package:ddblog/runner/counting.dart";
import "package:ddblog/runner/default.dart";
import "package:ddblog/runner/development.dart";
import "package:ddblog/runner/oneshot.dart";

Runner lookupRunner({String runModelName = "default", int countingRunnerParam = 1000}) {
  var mapping = <String, Runner>{
    "default": DefaultRunner(),
    "dev":  DevelopmentRunner(),
    "oneshot": OneshotRunner(),
    "count": CountingRunner(countingRunnerParam)
  };

  if (!mapping.containsKey(runModelName)) {
    throw ArgumentError("Illegal runner name: ${runModelName}");
  }

  return mapping[runModelName];
}