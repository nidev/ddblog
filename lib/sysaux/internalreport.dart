
/// This file declares an interface for internal reporting
/// like error, boot-up log, etc.
abstract class InternalReport {
  String _digest;
  Iterator<String> export();
  String get digest => _digest;
}

class EmptyReportObject implements Exception {}

class BootupReport extends InternalReport {
  bool _finalized = false;
  List<String> _buffer = List();

  @override
  Iterator<String> export() {
    return _buffer.iterator;
  }



}