@TestOn("vm")
import "dart:mirrors";
import "package:test/test.dart";
import "package:ddblog/platform_util.dart";
import "package:ddblog/logger.dart";
import "package:ddblog/log_writer/interface.dart";


/// Specially crafted LogWriter for testing.
/// The instance of this class does print nothing out but store messages in internal buffer.
/// Use message getter to get last written one. 
class DummyLogWriter extends LogWriter {
  String _buffer = null;

  String get buffer {
    return _buffer;
  }

  void clearBufferProperty() {
    _buffer = null;
  }

  @override
  void write(String msg) {
    _buffer = msg;
  }

  @override
  void writeLn(String msg) {
    _buffer = msg + PlatformUtil.newLine;
  }
}

void main() {
  group("'Constants validation' test", () {
    test("Has 4 enum values in LogLevel", () {
      expect(LogLevel.values.length, equals(4));
    });

    test("Has 4 keys in LogLevelFromString", () {
      expect(LogLevelFromString.length, equals(4));
    });

    test("Has string keys on LogLevelFromString", () {
      ["debug", "notify", "warn", "error"].forEach((name) => expect(LogLevelFromString.containsKey(name), equals(true)));
    });

    test("Has exact matched enum value in LogLevelFromString", () {
      expect(LogLevelFromString["debug"], equals(LogLevel.debug));
      expect(LogLevelFromString["notify"], equals(LogLevel.notify));
      expect(LogLevelFromString["warn"], equals(LogLevel.warn));
      expect(LogLevelFromString["error"], equals(LogLevel.error));
    });
  });

  group("'Logger class global logLevels feature' test", () {
    test("Has all available logLevles enabled", () {
      LogLevel.values.forEach(
        (logLvEnum) =>
          expect(Logger.logLevels.contains(logLvEnum), equals(true)));
    });
  });

  group("'Logger factory (single instance per tag)' test", () {
    final String testLoggerTag = "testLogger";
    final String otherTestLoggerTag = "examLogger";

    Logger testLogger;
    setUp(() => testLogger = Logger(testLoggerTag));

    test("Checks same hashCode on previous and logger on same tag", () {
      Logger testLogger_sameTag = Logger(testLoggerTag);
      expect(testLogger.hashCode, equals(testLogger_sameTag.hashCode));
    });

    test("Checks same hashCode on previous and logger on same tag", () {
      Logger testLogger_differentTag = Logger(otherTestLoggerTag);
      expect(testLogger.hashCode, isNot(equals(testLogger_differentTag.hashCode)));
    });

    tearDown(() => testLogger = null);
  });

  group("'Logging level control' test", () {
    final String testLoggerTag = "testLogger_logLevel";
    final Map<LogLevel, Symbol> reflectionSyms = {
      LogLevel.debug: #d,
      LogLevel.notify: #n,
      LogLevel.warn: #w,
      LogLevel.error: #e,
    };

    DummyLogWriter dummyLogWriter = DummyLogWriter();
    Logger testLogger = Logger(testLoggerTag, logWriter: dummyLogWriter);


    setUp(() => Logger.logLevels.clear());

    test("Logs nothing when no level is enabled", () {
      expect(Logger.logLevels.isEmpty, isTrue);

      InstanceMirror im = reflect(testLogger);
      reflectionSyms.values.forEach((symbol) {
        dummyLogWriter.clearBufferProperty();
        
        im.invoke(symbol, [""]);
        expect(dummyLogWriter._buffer, isNull);
      });
    });

    test("Logs only that level when that is the only one enabled", () { 
      reflectionSyms.forEach((lvEnum, logFnSym) {
        Logger.logLevels.clear();
        expect(Logger.logLevels.isEmpty, isTrue);

        Logger.logLevels.add(lvEnum);
        expect(Logger.logLevels.contains(lvEnum), isTrue);

        InstanceMirror im = reflect(testLogger);
        reflectionSyms.values.forEach((symbol) {
          dummyLogWriter.clearBufferProperty();
          
          im.invoke(symbol, ["A"]);
          if (symbol == logFnSym) {
            expect(dummyLogWriter._buffer, allOf(isNotNull));
          }
          else {
            expect(dummyLogWriter._buffer, isNull);
          }
        });
      });
    });

    test("Logs something when raw logging is called even if no level is enabled", () {
      Logger.logLevels.clear();
      expect(Logger.logLevels.isEmpty, isTrue);
      dummyLogWriter.clearBufferProperty();

      const testMessage = "RAW LOGGING";

      testLogger.r(testMessage);
      expect(dummyLogWriter._buffer, contains(testMessage));
    });
  });
}